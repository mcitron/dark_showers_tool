## make plots inline - for ipython notebooks

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams, colors
import os 

# Make use of TeX\ufeff
rc('text',usetex=True)
# Change all fonts to 'Computer Modern'
rc('font',**{'size':16, 'family':'serif','serif':['Times New Roman']})


# Change all fonts to 'Computer Modern'
rc('font',**{'size':16, 'family':'serif','serif':['Times New Roman']})
rc('xtick.major', size=5, pad=10)
rc('xtick', labelsize=16)
rc('ytick.major', size=5, pad=10)
rc('ytick', labelsize=16)

# make a dark shower object for each decay portal. Higgs portal is not implemented yet
from dark_shower import dark_shower
photon_portal=dark_shower(portal="photon")
gluon_portal=dark_shower(portal="gluon")
higgs_portal=dark_shower(portal="higgs")
darkphoton_portal=dark_shower(portal="darkphoton")
vector_portal=dark_shower(portal="vector")

# print(photon_portal.decay_PDG_codes.keys())
print(gluon_portal.decay_PDG_codes.keys())
# print(darkphoton_portal.decay_PDG_codes.keys())
# print(vector_portal.decay_PDG_codes.keys())

# Computes ctau in cm for a choice of mass and coupling, in this case the kinetic mixing parameter \epsilon. 
# To make use of this properly, we would probably need to finish the paper 
# Likely mostly useful for theorists 
# vector_portal.ctau(1.0,1e-4)

# Computes the minimum (well motivated) lifetime as a function of mass
massOptions = [2.,5.,10.,15.,20.]
# massOptions = [1.,4.,6.,10.,20.]
#portals = [higgs_portal,darkphoton_portal,vector_portal]
portals = [photon_portal,gluon_portal,higgs_portal,darkphoton_portal,vector_portal][-1:]
import math
from bisect import bisect_left
ctauOpts = [0.1,1,5,10,50]
#ctauOpts = [0.1]
# ctauOpts = [50,100,500,1000]
total = 0
massAndCtauDict = {}
for portal in portals:
    massAndCtauDict[portal] = {}
    min_mass = 2.
    while (portal.ctau_min(min_mass) > 100):
        min_mass += 1
    # print (portal.portal,min_mass)
    max_mass = 20
    while (math.isnan(portal.ctau_min(max_mass)) or portal.ctau_min(max_mass) > 100):
        max_mass -= 1
    # print (max_mass,portal.portal,portal.ctau_min(max_mass))
    # print (portal.portal,portal.ctau_min(max_mass))
    print (portal.portal)
    for mass in [min_mass] + list(range(int(math.ceil(min_mass/5.)*5),int(max_mass)+1,5)):
        index = bisect_left(ctauOpts,portal.ctau_min(mass))
        ctauOptsPerMass = ctauOpts[index:]
        massAndCtauDict[portal][mass] = ctauOptsPerMass
        print (str(mass)+" "+", ".join([str(x) for x in ctauOptsPerMass]))

# m_phi: mass of the dark pion, in GeV
# xi_omega=mass dark omega/ m_phi. You expect this parameter to be x_omega>1
# xi_Lambda=mass dark omega/ m_phi. You expect this parameter to be x_omega>1
# ctau: proper lifetime of the unstable meson, in cm. For the vector portal the omega is unstable, for the others the pion decays
# Nc: number of colors, should be positive integer
# Nf: number of flavors, should be positive integer
# mH: mass of the heavy particle (e.g. Higgs) that is decaying into the dark sector
# mayDecay: allow the hidden sector particles to decay. Default is "True", "False" is useful for debugging mostly 
# userName: name of person generating the pythia card
# It will print a warning if it thinks the lifetime is too low to be theoretically well motivated
# print ("gluon")
# gluon_portal.cmssw_cfg("test_gluon_m_phi_{0}_ctau_{1}.py",
#                          m_phi=1.0,
#                          xi_omega=1.0,
#                          xi_Lambda=1.0,
#                          ctau=2.0,
#                          Nc=3,
#                          Nf=1,
#                          mH=125,
#                          mayDecay=True,
#                          userName="Matthew")
# outputDir = "cfgs_extended_filter_2017_2018/"
outputDir = "cfgs_bpark_lowctau_noFilter_ctaumm_2022/"
outputDirSlha = "slha_bpark_lowctau_noFilter_ctaumm_2018/"
models = []
year = 2018
filterPt = False
if not os.path.exists(outputDir):
    os.mkdir(outputDir)
if not os.path.exists(outputDirSlha):
    os.mkdir(outputDirSlha)
for portal in portals:
    for mass,ctaus in massAndCtauDict[portal].items():
        for ctau in ctaus:
            if ctau*10 >= 1:
                ctauS = str(int(ctau*10))
            else:
                ctauS = str(ctau*1).replace(".","p")
            models.append([portal.portal,str(int(mass)),ctauS,"1","1"])
            portal.cmssw_cfg(outputDir+"hiddenValleyGridPack_{0}_m_{1}_ctau_{2}_xiO_{3}_xiL_{4}.py".format(portal.portal,int(mass),ctauS,1,1,year),
                         m=mass,
                         xi_omega=1.0,
                         xi_Lambda=1.0,
                         ctau=ctau,
                         Nc=3,
                         Nf=1,
                         mH=125,
                         mayDecay=True,
                         userName="Matthew",
                         year=year,
                         filterPt=filterPt
                         )
            if portal.portal == "vector":
                portal.pythia_card(outputDirSlha+"hiddenValleyGridPack_{0}_m_{1}_ctau_{2}_xiO_{3}_xiL_{4}.slha".format(portal.portal,int(mass),ctauS,1,1,year),
                             m=mass,
                             xi_omega=1.0,
                             xi_Lambda=1.0,
                             ctau=ctau,
                             Nc=3,
                             Nf=1,
                             mH=125,
                             mayDecay=True,
                             userName="Matthew",
                            year = year
                             )
            if portal.portal != "vector":
                models.append([portal.portal,str(int(mass)),ctauS,"2p5","1"])
                portal.cmssw_cfg(outputDir+"/hiddenValleyGridPack_{0}_m_{1}_ctau_{2}_xiO_{3}_xiL_{4}.py".format(portal.portal,int(mass),ctauS,"2p5",1,year),
                             m=mass,
                             xi_omega=2.5,
                             xi_Lambda=1.0,
                             ctau=ctau,
                             Nc=3,
                             Nf=1,
                             mH=125,
                             mayDecay=True,
                             userName="Matthew",
                             year = year,
                            filterPt=filterPt
                             )
                models.append([portal.portal,str(int(mass)),ctauS,"2p5","2p5"])
                portal.cmssw_cfg(outputDir+"hiddenValleyGridPack_{0}_m_{1}_ctau_{2}_xiO_{3}_xiL_{4}.py".format(portal.portal,int(mass),ctauS,"2p5","2p5",year),
                             m=mass,
                             xi_omega=2.5,
                             xi_Lambda=2.5,
                             ctau=ctau,
                             Nc=3,
                             Nf=1,
                             mH=125,
                             mayDecay=True,
                             userName="Matthew",
                             year=year,
                            filterPt=filterPt
                             )
with open (outputDir+"/models.txt","w") as modelsFile:
    modelsFile.write(",".join(["portal","mass","ctau","xi","xl"])+"\n")
    for line in models:
        modelsFile.write(",".join(line)+"\n")
# print ("dark photon")
# darkphoton_portal.cmssw_cfg("test_darkphoton_m_phi_{0}_ctau_{1}.py",
#                          m_phi=1.0,
#                          xi_omega=1.0,
#                          xi_Lambda=1.0,
#                          ctau=2.0,
#                          Nc=3,
#                          Nf=1,
#                          mH=125,
#                          mayDecay=True,
#                          userName="Matthew")
# print ("photon")
# photon_portal.cmssw_cfg("test_photon_m_phi_{0}_ctau_{1}.py",
#                          m_phi=1.0,
#                          xi_omega=1.0,
#                          xi_Lambda=1.0,
#                          ctau=2.0,
#                          Nc=3,
#                          Nf=1,
#                          mH=125,
#                          mayDecay=True,
#                          userName="Matthew")
