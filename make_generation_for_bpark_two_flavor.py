import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams, colors
from dark_shower_two_flavor import dark_shower_two_flavor
from scipy.optimize import brentq
import math


def set_epsilonA(epsilonLog,scenario,ctau):
    scenario.update_params(m_pi2=scenario.m_pi2,
        m_eta=scenario.m_eta,
        Lambda=scenario.Lambda,
        sin_theta=scenario.sin_theta,
        mA=scenario.mA,
        g=scenario.g,
        epsilon=10**epsilonLog)
    return (scenario.A_ctau-ctau)
def set_gPi3(gLog,scenario,ctau):
    scenario.update_params(m_pi2=scenario.m_pi2,
        m_eta=scenario.m_eta,
        Lambda=scenario.Lambda,
        sin_theta=scenario.sin_theta,
        mA=scenario.mA,
        g=10**gLog,
        epsilon=scenario.epsilon)
    return (scenario.pi3_ctau-ctau)
def set_gEta(gLog,scenario,ctau):
    scenario.update_params(m_pi2=scenario.m_pi2,
        m_eta=scenario.m_eta,
        Lambda=scenario.Lambda,
        sin_theta=scenario.sin_theta,
        mA=scenario.mA,
        g=10**gLog,
        epsilon=scenario.epsilon)
    return (scenario.eta_ctau-ctau)
def set_epsilonEta(epsilonLog,scenario,ctau):
    scenario.update_params(m_pi2=scenario.m_pi2,
        m_eta=scenario.m_eta,
        Lambda=scenario.Lambda,
        sin_theta=scenario.sin_theta,
        mA=scenario.mA,
        g=scenario.g,
        epsilon=10**epsilonLog)
    return (scenario.eta_ctau-ctau)

outDir = "cfgs_2022_fixB1"
if not os.path.exists(outDir):
    os.mkdir(outDir)
outDirSlha = "slhas"
if not os.path.exists(outDir):
    os.mkdir(outDir)

models = [] 
year=2022
for m_pi2 in [1,2,4,10]:
    for m_A in [m_pi2/10,m_pi2/3]:
        if (m_A < 2*0.106): continue
        scenarioA=dark_shower_two_flavor(
            m_pi2=m_pi2,
            m_eta=4*m_pi2,
            Lambda=4*m_pi2,
            sin_theta=0.1,
            mA=m_A,
            g=0.09,
            epsilon=5e-4)
        for ctau in [0.01,0.1,1,10]:
            epsilon = brentq(set_epsilonA, -3,-10,args=(scenarioA,ctau))
            scenarioA.cmssw_cfg(os.getcwd()+"/"+outDir+"/"+"scenarioA_mpi_{}_mA_{}_ctau_{}.py".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")),year=year)
            scenarioA.pythia_card(os.getcwd()+"/"+outDirSlha+"/"+"scenarioA_mpi_{}_mA_{}_ctau_{}.slha".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")))
            models.append(["scenarioA",str(m_pi2),("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")])
for m_pi2 in [1,2,4]:
    for m_A in [m_pi2/5,m_pi2/3]:
        if (m_A < 2*0.106): continue
        scenarioB1=dark_shower_two_flavor(
            m_pi2=m_pi2,
            m_eta=4*m_pi2,
            Lambda=4*m_pi2,
            sin_theta=0.1,
            mA=m_A,
            g=1E-2,
            epsilon=0.1)
        for ctau in [0.01,0.1,1,10]:
            g = brentq(set_gPi3, -1,-6,args=(scenarioB1,ctau))
            scenarioB1.cmssw_cfg(os.getcwd()+"/"+outDir+"/"+"scenarioB1_mpi_{}_mA_{}_ctau_{}.py".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")),year=year)
            scenarioB1.pythia_card(os.getcwd()+"/"+outDirSlha+"/"+"scenarioB1_mpi_{}_mA_{}_ctau_{}.slha".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")))
            models.append(["scenarioB1",str(m_pi2),("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")])

for m_pi2 in [1,2,4]:
    for m_A in [m_pi2/2+0.1]:
        if (m_A < 2*0.106): continue
        scenarioB2=dark_shower_two_flavor(
            m_pi2=m_pi2,
            m_eta=2.5*m_pi2,
            Lambda=2.5*m_pi2,
            sin_theta=0.1,
            mA=m_A,
            g=1E-6,
            epsilon=0.1)
        for ctau in [0.01,0.1,1,10]:
            g = brentq(set_gEta, -2,-10,args=(scenarioB2,ctau))
            scenarioB2.cmssw_cfg(os.getcwd()+"/"+outDir+"/"+"scenarioB2_mpi_{}_mA_{}_ctau_{}.py".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")),year=year)
            scenarioB2.pythia_card(os.getcwd()+"/"+outDirSlha+"/"+"scenarioB2_mpi_{}_mA_{}_ctau_{}.slha".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")))
            models.append(["scenarioB2",str(m_pi2),("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")])
for m_pi2 in [2,4,10]:
    for m_A in [m_pi2/1.25]:
        if (m_A < 2*0.106): continue
        scenarioC=dark_shower_two_flavor(
            m_pi2=m_pi2,
            m_eta=(m_pi2+m_A)/1.25,
            Lambda=(m_pi2+m_A)/1.25,
            sin_theta=0.1,
            mA=m_A,
            g=0.1,
            epsilon=10**-0.5)
        for ctau in [0.01,0.1,1,10]:
            g = brentq(set_epsilonEta, 0,-10,args=(scenarioC,ctau))
            scenarioC.cmssw_cfg(os.getcwd()+"/"+outDir+"/"+"scenarioC_mpi_{}_mA_{}_ctau_{}.py".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")),year=year)
            scenarioC.pythia_card(os.getcwd()+"/"+outDirSlha+"/"+"scenarioC_mpi_{}_mA_{}_ctau_{}.slha".format(m_pi2,("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")))
            models.append(["scenarioC",str(m_pi2),("%.2f"%m_A).replace(".","p"),str(10*ctau).replace(".","p")])
with open(outDir+"/models_newBenchmarks.txt","w") as f:
    for line in models:
        f.write(",".join(line)+"\n")

